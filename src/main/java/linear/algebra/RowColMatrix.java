package linear.algebra;

import linear.algebra.throwable.MatrixSizeThrowable;

public class RowColMatrix {
    public double[][] generateRowColMatrtix(int rowSize, int colSize) throws MatrixSizeThrowable {
        if (rowSize <= 0) {
            throw new MatrixSizeThrowable("RowColMatrix rowSize should be more than 0. And the current rowSize is " + rowSize);
        }
        if (colSize <= 0) {
            throw new MatrixSizeThrowable("RowColMatrix columnSise should be more than 0. And the current columnSise is " + colSize);
        }

        double[][] resultRowColMatrix = new double[rowSize][colSize];

        for (int i = 0; i < resultRowColMatrix.length; i++) {
            for (int j = 0; j < resultRowColMatrix[0].length; j++) {
                resultRowColMatrix[i][j] = Math.random();
            }
        }

        return resultRowColMatrix;
    }

    public double[][] ijkRowColMatrtixMultiplication(double[][] aMatrix, double[][] bMatrix) throws MatrixSizeThrowable {
        if (aMatrix[0].length != bMatrix.length) {
            throw new MatrixSizeThrowable("aMat colmunSize should be same bMat rowSize.\n"
                    + "aMat columnSize = " + aMatrix[0].length + "\n"
                    + "bMat rowSize = " + bMatrix.length);
        }
        int n = aMatrix[0].length;

        double[][] resultMatrix = new double[aMatrix.length][bMatrix[0].length];

        for (int i = 0; i < aMatrix.length; i++) {
            for (int j = 0; j < bMatrix[0].length; j++) {
                for (int k = 0; k < n; k++) {
                    resultMatrix[i][j] += aMatrix[i][k] * bMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }

    public double[][] ikjRowColMatrtixMultiplication(double[][] aMatrix, double[][] bMatrix) throws MatrixSizeThrowable {
        if (aMatrix[0].length != bMatrix.length) {
            throw new MatrixSizeThrowable("aMat colmunSize should be same bMat rowSize.\n"
                    + "aMat columnSize = " + aMatrix[0].length + "\n"
                    + "bMat rowSize = " + bMatrix.length);
        }
        int n = aMatrix[0].length;

        double[][] resultMatrix = new double[aMatrix.length][bMatrix[0].length];

        for (int i = 0; i < aMatrix.length; i++) {
            for (int k = 0; k < n; k++) {
                for (int j = 0; j < bMatrix[0].length; j++) {
                    resultMatrix[i][j] += aMatrix[i][k] * bMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }

    public double[][] jikRowColMatrtixMultiplication(double[][] aMatrix, double[][] bMatrix) throws MatrixSizeThrowable {
        if (aMatrix[0].length != bMatrix.length) {
            throw new MatrixSizeThrowable("aMat colmunSize should be same bMat rowSize.\n"
                    + "aMat columnSize = " + aMatrix[0].length + "\n"
                    + "bMat rowSize = " + bMatrix.length);
        }
        int n = aMatrix[0].length;

        double[][] resultMatrix = new double[aMatrix.length][bMatrix[0].length];

        for (int j = 0; j < bMatrix[0].length; j++) {
            for (int i = 0; i < aMatrix.length; i++) {
                for (int k = 0; k < n; k++) {
                    resultMatrix[i][j] += aMatrix[i][k] * bMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }

    public double[][] jkiRowColMatrtixMultiplication(double[][] aMatrix, double[][] bMatrix) throws MatrixSizeThrowable {
        if (aMatrix[0].length != bMatrix.length) {
            throw new MatrixSizeThrowable("aMat colmunSize should be same bMat rowSize.\n"
                    + "aMat columnSize = " + aMatrix[0].length + "\n"
                    + "bMat rowSize = " + bMatrix.length);
        }
        int n = aMatrix[0].length;

        double[][] resultMatrix = new double[aMatrix.length][bMatrix[0].length];

        for (int j = 0; j < bMatrix[0].length; j++) {
            for (int k = 0; k < n; k++) {
                for (int i = 0; i < aMatrix.length; i++) {
                    resultMatrix[i][j] += aMatrix[i][k] * bMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }

    public double[][] kijRowColMatrtixMultiplication(double[][] aMatrix, double[][] bMatrix) throws MatrixSizeThrowable {
        if (aMatrix[0].length != bMatrix.length) {
            throw new MatrixSizeThrowable("aMat colmunSize should be same bMat rowSize.\n"
                    + "aMat columnSize = " + aMatrix[0].length + "\n"
                    + "bMat rowSize = " + bMatrix.length);
        }
        int n = aMatrix[0].length;

        double[][] resultMatrix = new double[aMatrix.length][bMatrix[0].length];

        for (int k = 0; k < n; k++) {
            for (int i = 0; i < aMatrix.length; i++) {
                for (int j = 0; j < bMatrix[0].length; j++) {
                    resultMatrix[i][j] += aMatrix[i][k] * bMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }

    public double[][] kjiRowColMatrtixMultiplication(double[][] aMatrix, double[][] bMatrix) throws MatrixSizeThrowable {
        if (aMatrix[0].length != bMatrix.length) {
            throw new MatrixSizeThrowable("aMat colmunSize should be same bMat rowSize.\n"
                    + "aMat columnSize = " + aMatrix[0].length + "\n"
                    + "bMat rowSize = " + bMatrix.length);
        }
        int n = aMatrix[0].length;

        double[][] resultMatrix = new double[aMatrix.length][bMatrix[0].length];

        for (int k = 0; k < n; k++) {
            for (int j = 0; j < bMatrix[0].length; j++) {
                for (int i = 0; i < aMatrix.length; i++) {
                    resultMatrix[i][j] += aMatrix[i][k] * bMatrix[k][j];
                }
            }
        }

        return resultMatrix;
    }

    public double[] convertDoubleArrayToArray(double[][] rowColMatrix) {
        int rowSize = rowColMatrix.length;
        int colSize = rowColMatrix[0].length;
        double[] resultRowMatrix = new double[rowSize * colSize];
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < colSize; j++) {
                resultRowMatrix[i*colSize + j] = rowColMatrix[i][j];
            }
        }
        return resultRowMatrix;
    }
}