package linear.algebra;

import linear.algebra.throwable.MatrixSizeThrowable;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.StopWatch;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RowMatrixTest {
    private StopWatch stopWatch;
    private RowMatrix matrix;
    private int aMatrixRowSize;
    private int aMatrixColSize;
    private int bMatrixColSize;
    private double[] aMatrix;
    private double[] bMatrix;
    private double[] cMatrixByijk;
    private double[] cMatrixByikj;
    private double[] cMatrixByjik;
    private double[] cMatrixByjki;
    private double[] cMatrixBykij;
    private double[] cMatrixBykji;
    private double cValueByijk;
    private double cValueByikj;
    private double cValueByjik;
    private double cValueByjki;
    private double cValueBykij;
    private double cValueBykji;

    @Before
    public void setUp() throws MatrixSizeThrowable {
        stopWatch = new StopWatch("RowMatrix Multiplication");
        matrix = new RowMatrix();
        aMatrixRowSize = 400;
        aMatrixColSize = 400;
        bMatrixColSize = 400;
        aMatrix = matrix.generateRowMatrix(aMatrixRowSize, aMatrixColSize);
        bMatrix = matrix.generateRowMatrix(aMatrixColSize, bMatrixColSize);
    }

    @Test
    public void rowMatrixMultiplicationTest() {
        stopWatch.start("ijk RowMatix Multiplication");
        cMatrixByijk = matrix.ijkRowMatirxMultiplication(aMatrix, bMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("ikj RowMatix Multiplication");
        cMatrixByikj = matrix.ikjRowMatirxMultiplication(aMatrix, bMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("jik RowMatix Multiplication");
        cMatrixByjik = matrix.jikRowMatirxMultiplication(aMatrix, bMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("jki RowMatix Multiplication");
        cMatrixByjki = matrix.jkiRowMatirxMultiplication(aMatrix, bMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("kij RowMatix Multiplication");
        cMatrixBykij = matrix.kijRowMatirxMultiplication(aMatrix, bMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("kji RowMatix Multiplication");
        cMatrixBykji = matrix.kjiRowMatirxMultiplication(aMatrix, bMatrix, aMatrixColSize);
        stopWatch.stop();

        for (int i = 0; i < aMatrixRowSize; i++) {
            for (int j = 0; j < bMatrixColSize; j++) {
                cValueByijk = cMatrixByijk[i * bMatrixColSize + j];
                cValueByikj = cMatrixByikj[i * bMatrixColSize + j];
                cValueByjik = cMatrixByjik[i * bMatrixColSize + j];
                cValueByjki = cMatrixByjki[i * bMatrixColSize + j];
                cValueBykij = cMatrixBykij[i * bMatrixColSize + j];
                cValueBykji = cMatrixBykji[i * bMatrixColSize + j];

                assertThat("Compare RowMatrix[" + i + "][" + j + "] is different", cValueByijk - cValueByikj, is(0.0));
                assertThat("Compare RowMatrix[" + i + "][" + j + "] is different", cValueByijk - cValueByjik, is(0.0));
                assertThat("Compare RowMatrix[" + i + "][" + j + "] is different", cValueByijk - cValueByjki, is(0.0));
                assertThat("Compare RowMatrix[" + i + "][" + j + "] is different", cValueByijk - cValueBykij, is(0.0));
                assertThat("Compare RowMatrix[" + i + "][" + j + "] is different", cValueByijk - cValueBykji, is(0.0));
            }
        }

        System.out.println(stopWatch.prettyPrint());
    }
}