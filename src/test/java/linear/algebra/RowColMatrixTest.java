package linear.algebra;

import linear.algebra.throwable.MatrixSizeThrowable;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.StopWatch;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RowColMatrixTest {
    private StopWatch stopWatch;
    private RowColMatrix matrix;
    private int aMatrixRowSize;
    private int aMatrixColSize;
    private int bMatrixColSize;
    private double[][] aMatrix;
    private double[][] bMatrix;
    private double[][] cMatrixByijk;
    private double[][] cMatrixByikj;
    private double[][] cMatrixByjik;
    private double[][] cMatrixByjki;
    private double[][] cMatrixBykij;
    private double[][] cMatrixBykji;
    private double ijkValue;
    private double ikjValue;
    private double jikValue;
    private double jkiValue;
    private double kijValue;
    private double kjiValue;

    @Before
    public void setUp() throws MatrixSizeThrowable {
        stopWatch = new StopWatch("RowColMatrix Multiplication");
        matrix = new RowColMatrix();
        aMatrixRowSize = 400;
        aMatrixColSize = 400;
        bMatrixColSize = 400;
        aMatrix = matrix.generateRowColMatrtix(aMatrixRowSize, aMatrixColSize);
        bMatrix = matrix.generateRowColMatrtix(aMatrixColSize, bMatrixColSize);
    }

    @Test
    public void rowColMatrtixMultiplication() throws MatrixSizeThrowable {
        stopWatch.start("ijk RowColMatrix Multiplication");
        cMatrixByijk = matrix.ijkRowColMatrtixMultiplication(aMatrix, bMatrix);
        stopWatch.stop();

        stopWatch.start("ikj RowColMatrix Multiplication");
        cMatrixByikj = matrix.ikjRowColMatrtixMultiplication(aMatrix, bMatrix);
        stopWatch.stop();

        stopWatch.start("jik RowColMatrix Multiplication");
        cMatrixByjik = matrix.jikRowColMatrtixMultiplication(aMatrix, bMatrix);
        stopWatch.stop();

        stopWatch.start("jki RowColMatrix Multiplication");
        cMatrixByjki = matrix.jkiRowColMatrtixMultiplication(aMatrix, bMatrix);
        stopWatch.stop();

        stopWatch.start("kij RowColMatrix Multiplication");
        cMatrixBykij = matrix.kijRowColMatrtixMultiplication(aMatrix, bMatrix);
        stopWatch.stop();

        stopWatch.start("kji RowColMatrix Multiplication");
        cMatrixBykji = matrix.kjiRowColMatrtixMultiplication(aMatrix, bMatrix);
        stopWatch.stop();

        for (int i = 0; i < aMatrixRowSize; i++) {
            for (int j = 0; j < bMatrixColSize; j++) {
                ijkValue = cMatrixByijk[i][j];
                ikjValue = cMatrixByikj[i][j];
                jikValue = cMatrixByjik[i][j];
                jkiValue = cMatrixByjki[i][j];
                kijValue = cMatrixBykij[i][j];
                kjiValue = cMatrixBykji[i][j];

                assertThat("Compare RowColMatrix[" + i + "][" + j + "] is different", ijkValue - ikjValue, is(0.0));
                assertThat("Compare RowColMatrix[" + i + "][" + j + "] is different", ijkValue - jikValue, is(0.0));
                assertThat("Compare RowColMatrix[" + i + "][" + j + "] is different", ijkValue - jkiValue, is(0.0));
                assertThat("Compare RowColMatrix[" + i + "][" + j + "] is different", ijkValue - kijValue, is(0.0));
                assertThat("Compare RowColMatrix[" + i + "][" + j + "] is different", ijkValue - kjiValue, is(0.0));
            }
        }

        System.out.println(stopWatch.prettyPrint());
    }

    @Test
    public void testConvertDoubleArrayToArray() {
        double[][] beforeConvertedMatrix = new double[][]{{1.0, 2.0, 3.0}, {4.0, 5.0, 6.0}, {7.0, 8.0, 9.0}, {10.0, 11.0, 12.0}};
        double[] afterConvertedMatirx = matrix.convertDoubleArrayToArray(beforeConvertedMatrix);
        double[] expectMatrix = new double[]{1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0};
        for (int i = 0; i < expectMatrix.length; i++) {
            assertThat("Matrix [" + i + "] is different", expectMatrix[i], is(afterConvertedMatirx[i]));
        }
    }
}