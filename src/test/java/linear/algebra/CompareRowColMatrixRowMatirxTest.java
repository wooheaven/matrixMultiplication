package linear.algebra;

import linear.algebra.throwable.MatrixSizeThrowable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.StopWatch;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CompareRowColMatrixRowMatirxTest {
    private static StopWatch stopWatch;
    private static RowColMatrix rowColMatrix;
    private static RowMatrix rowMatrix;
    private static int aMatrixRowSize;
    private static int aMatrixColSize;
    private static int bMatrixColSize;
    private static double[][] aRowColMatrix;
    private static double[][] bRowColMatrix;
    private static double[] aRowMatrix;
    private static double[] bRowMatrix;
    private static double[][] cRowColMatrixByijk;
    private static double[][] cRowColMatrixByikj;
    private static double[][] cRowColMatrixByjik;
    private static double[][] cRowColMatrixByjki;
    private static double[][] cRowColMatrixBykij;
    private static double[][] cRowColMatrixBykji;
    private static double[] cRowMatrixByijk;
    private static double[] cRowMatrixByikj;
    private static double[] cRowMatrixByjik;
    private static double[] cRowMatrixByjki;
    private static double[] cRowMatrixBykij;
    private static double[] cRowMatrixBykji;

    @BeforeClass
    public static void setUp() throws MatrixSizeThrowable {
        stopWatch = new StopWatch("Compare Multiplication by RowColMatrix and RowMatirx");
        rowColMatrix = new RowColMatrix();
        rowMatrix = new RowMatrix();
        aMatrixRowSize = 800;
        aMatrixColSize = 700;
        bMatrixColSize = 800;
        aRowColMatrix = rowColMatrix.generateRowColMatrtix(aMatrixRowSize, aMatrixColSize);
        bRowColMatrix = rowColMatrix.generateRowColMatrtix(aMatrixColSize, bMatrixColSize);
        aRowMatrix = rowColMatrix.convertDoubleArrayToArray(aRowColMatrix);
        bRowMatrix = rowColMatrix.convertDoubleArrayToArray(bRowColMatrix);

        stopWatch.start("ijk RowColMatrix Multiplication");
        cRowColMatrixByijk = rowColMatrix.ijkRowColMatrtixMultiplication(aRowColMatrix, bRowColMatrix);
        stopWatch.stop();

        stopWatch.start("ijk RowMatrix Multiplication");
        cRowMatrixByijk = rowMatrix.ijkRowMatirxMultiplication(aRowMatrix, bRowMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("ikj RowColMatrix Multiplication");
        cRowColMatrixByikj = rowColMatrix.ikjRowColMatrtixMultiplication(aRowColMatrix, bRowColMatrix);
        stopWatch.stop();

        stopWatch.start("ikj RowMatrix Multiplication");
        cRowMatrixByikj = rowMatrix.ikjRowMatirxMultiplication(aRowMatrix, bRowMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("jik RowColMatrix Multiplication");
        cRowColMatrixByjik = rowColMatrix.jikRowColMatrtixMultiplication(aRowColMatrix, bRowColMatrix);
        stopWatch.stop();

        stopWatch.start("jik RowMatrix Multiplication");
        cRowMatrixByjik = rowMatrix.jikRowMatirxMultiplication(aRowMatrix, bRowMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("jki RowColMatrix Multiplication");
        cRowColMatrixByjki = rowColMatrix.jkiRowColMatrtixMultiplication(aRowColMatrix, bRowColMatrix);
        stopWatch.stop();

        stopWatch.start("jki RowMatrix Multiplication");
        cRowMatrixByjki = rowMatrix.jkiRowMatirxMultiplication(aRowMatrix, bRowMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("kij RowColMatrix Multiplication");
        cRowColMatrixBykij = rowColMatrix.kijRowColMatrtixMultiplication(aRowColMatrix, bRowColMatrix);
        stopWatch.stop();

        stopWatch.start("kij RowMatrix Multiplication");
        cRowMatrixBykij = rowMatrix.kijRowMatirxMultiplication(aRowMatrix, bRowMatrix, aMatrixColSize);
        stopWatch.stop();

        stopWatch.start("kji RowColMatrix Multiplication");
        cRowColMatrixBykji = rowColMatrix.kjiRowColMatrtixMultiplication(aRowColMatrix, bRowColMatrix);
        stopWatch.stop();

        stopWatch.start("kji RowMatrix Multiplication");
        cRowMatrixBykji = rowMatrix.kjiRowMatirxMultiplication(aRowMatrix, bRowMatrix, aMatrixColSize);
        stopWatch.stop();
    }

    @AfterClass
    public static void testReport() {
        System.out.println(stopWatch.prettyPrint());
    }

    @Test
    public void testCompareRowColMatrixRowMatirx_ijk() throws MatrixSizeThrowable {
        double[] convertedRowMatrix = rowColMatrix.convertDoubleArrayToArray(cRowColMatrixByijk);
        for (int i = 0; i < cRowMatrixByijk.length; i++) {
            assertThat("Matrix [" + i + "] is different", convertedRowMatrix[i], is(cRowMatrixByijk[i]));
        }
    }

    @Test
    public void testCompareRowColMatrixRowMatirx_ikj() throws MatrixSizeThrowable {
        double[] convertedRowMatrix = rowColMatrix.convertDoubleArrayToArray(cRowColMatrixByikj);
        for (int i = 0; i < cRowMatrixByikj.length; i++) {
            assertThat("Matrix [" + i + "] is different", convertedRowMatrix[i], is(cRowMatrixByikj[i]));
            assertThat("Matrix [" + i + "] is different", cRowMatrixByijk[i], is(cRowMatrixByikj[i]));
        }
    }

    @Test
    public void testCompareRowColMatrixRowMatirx_jik() throws MatrixSizeThrowable {
        double[] convertedRowMatrix = rowColMatrix.convertDoubleArrayToArray(cRowColMatrixByjik);
        for (int i = 0; i < cRowMatrixByjik.length; i++) {
            assertThat("Matrix [" + i + "] is different", convertedRowMatrix[i], is(cRowMatrixByjik[i]));
            assertThat("Matrix [" + i + "] is different", cRowMatrixByikj[i], is(cRowMatrixByjik[i]));
        }
    }

    @Test
    public void testCompareRowColMatrixRowMatirx_jki() throws MatrixSizeThrowable {
        double[] convertedRowMatrix = rowColMatrix.convertDoubleArrayToArray(cRowColMatrixByjki);
        for (int i = 0; i < cRowMatrixByjki.length; i++) {
            assertThat("Matrix [" + i + "] is different", convertedRowMatrix[i], is(cRowMatrixByjki[i]));
            assertThat("Matrix [" + i + "] is different", cRowMatrixByjik[i], is(cRowMatrixByjki[i]));
        }
    }

    @Test
    public void testCompareRowColMatrixRowMatirx_kij() throws MatrixSizeThrowable {
        double[] convertedRowMatrix = rowColMatrix.convertDoubleArrayToArray(cRowColMatrixBykij);
        for (int i = 0; i < cRowMatrixBykij.length; i++) {
            assertThat("Matrix [" + i + "] is different", convertedRowMatrix[i], is(cRowMatrixBykij[i]));
            assertThat("Matrix [" + i + "] is different", cRowMatrixByjki[i], is(cRowMatrixBykij[i]));
        }
    }

    @Test
    public void testCompareRowColMatrixRowMatirx_kji() throws MatrixSizeThrowable {
        double[] convertedRowMatrix = rowColMatrix.convertDoubleArrayToArray(cRowColMatrixBykji);
        for (int i = 0; i < cRowMatrixBykji.length; i++) {
            assertThat("Matrix [" + i + "] is different", convertedRowMatrix[i], is(cRowMatrixBykji[i]));
            assertThat("Matrix [" + i + "] is different", cRowMatrixByjki[i], is(cRowMatrixBykji[i]));
        }
    }
}
